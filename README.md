Vanilla MC Server Tools
===============

This toolkit is a suite of tools for helping to manage vanilla minecraft servers that are kept running via a screen session. It allows you to run custom announcements at a specified interval, and to run automated backups.

To use this software you will need to compile your own custom jar at the moment. Assuming you have JRuby and bundler installed, use

	bundle install
	rake compile jar

to build your server tools. You can then run your jar by executing

	java -jar vmst.jar

Many new things will be making it's way into this toolkit such as IRC integration. If you have other ideas pleae file a new issue and mark it as enhancement, or if you arrived hear from the forum thread, voice it there.