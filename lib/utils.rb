require 'yaml'

class Utils

  def initialize
    @@config = YAML.load_file 'config.yaml'
    @@announcer = Announcer.new @@config
    @@backup = Backup.new
    @@irc_bot = IRC.new
  end

  def display_banner
    banner = "

    *******************************************************************************
    *                                                                             *
    *                   Welcome to VMST written by C0deMaver1ck                   *
    *                                                                             *
    *                        Vanilla Minecraft Server Tools                       *
    *                                                                             *
    *******************************************************************************
    "
    puts banner
  end

  def run(command)
    %x{screen -p 0 -X stuff "#{command} $(printf '\r')"}
  end

  def start_announcing
  	@@announcer.run!
  end

  def backup
  	@@backup.run!
  end

  def start_ircbot
    @@irc_bot.run!
  end
end