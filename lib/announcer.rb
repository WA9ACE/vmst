require 'yaml'

class Announcer < Utils

  def initialize(config)
    @interval = config['interval']
    @announcements = YAML.load_file 'announcements.yaml'
  end

  def run!
    while true do
      message = @announcements.sample
      announce message
      sleep @interval
    end
  end

  def announce(message)
    %x{screen -p 0 -X stuff "`printf "say #{message}\r"`"}
  end
end