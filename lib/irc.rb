require 'socket'

class IRC < Utils
  def initialize
  end

  def run!
    puts 'Connecting to IRC...'
    @socket = TCPSocket.open 'irc.freenode.net', 6667
    @nick = 'RogueCraft_Bot'
    @channel = '#roguecraft'
    @password = ''
    @socket.puts "USER testing 0 * testing"
    @socket.puts "NICK #{@nick}"
    @socket.puts "PASS #{@password}"
    @socket.puts "JOIN #{@channel}"
    @socket.puts "PRIVMSG #{@channel} :Starting up the relay."
    puts 'Connected.'

    Thread.new do
      sleep 15
      watch_log do |line|
        case line

        when /\[INFO\] (.*?) lost connection: (.*)/
          send "#{$1} lost connection: #{$2}"

        when /\[INFO\] <(.*?)> (.*)/
          send "#{$1}: #{$2}"

        when /\[INFO\] (.*?) (.*)/
          if $2.include? 'logged in'
            msg = $1.gsub(/\[.*\]/, '')
            send "#{msg} logged in"
          end
        end
      end
    end

    until @socket.eof? do
      raw = @socket.gets

      if raw.include? 'PING'
        @socket.puts 'PONG :'
      elsif raw.include?('freenode.net') || raw.include?('RogueCraft_Bot')
      else
        msg = raw_interpreter(raw)
        @@announcer.announce "#{msg}"
      end 
    end
  end

  def send msg
    @socket.puts "PRIVMSG #{@channel} :#{msg}"
  end

  def watch_log &block
    timeout = 1
    log = File.open '/home/c0demaver1ck/minecraft/server.log', 'r'
    log.seek 0, IO::SEEK_END
 
    while true do
      select [log]
      yield log.gets
      sleep timeout
    end
  end

  def parse_params(raw_params)
    raw_params = raw_params.strip
    params     = []

    if match = raw_params.match(/(?:^:| :)(.*)$/)
      params = match.pre_match.split(" ")
      params << match[1]
    else
      params = raw_params.split(" ")
    end

    return params
  end

  def raw_interpreter raw
    raw.inspect
    match = raw.match(/(^:(\S+) !)?(\S+)(.*)/)
    content = parse_params(match.captures[3])[2]
    name = match.captures[2].match(/.*!/).to_s.gsub(/!/, '').gsub(/\:/, '')
    msg = name + ': ' + content

    return msg
  end
end
