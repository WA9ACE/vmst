require 'fileutils'
require 'zip/zip'
require 'zip/zipfilesystem'

class Backup < Utils
  def initialize
    @filename = "h" + Time.now.hour.to_s + "d" + Time.now.day.to_s + "m" + Time.now.month.to_s
  end

  def run!
    @@announcer.announce "Backup Starting"
    Utils.run "save-off"
    Utils.run "save-all"
    backup = compress(@config['backup_folder'])
    FileUtils.move @filename, @config['move_folder']
    @@announcer.announce "Backup Complete"
    Utils.run "save-on"
  end

  def compress(path)
    # TODO: Compression code
  end
end